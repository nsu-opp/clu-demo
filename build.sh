#!/bin/bash
set -x

mpicc -std=gnu99 -O3 -Wpedantic -Wall -Werror -Wextra -o clu-demo main.c
