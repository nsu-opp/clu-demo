#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>


#define RANK_ROOT 0


typedef struct {
    MPI_Comm comm;
    int size;
    int rank;
} CommInfo;


int run(CommInfo comm_info);


int main(
        int argc,
        char ** argv) {
    int size, rank;
    char name[MPI_MAX_PROCESSOR_NAME];
    int name_length;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name(name, &name_length);
    name[name_length] = '\0';

    const double start_time_s = MPI_Wtime();
    const int exit_code = run((CommInfo) { .comm = MPI_COMM_WORLD, .size=size, .rank=rank });
    const double end_time_s = MPI_Wtime();

    MPI_Finalize();

    printf("Process %d running on %s took %.2lfs\n", rank, name, end_time_s - start_time_s);
    return exit_code;
}


#define DUMMY_DATA_SIZE (10 * 1024 * 1024 / sizeof(int))
#define MAX_ITER 25
#define DUMMY_DATA_TAG 0


int run(const CommInfo comm_info) {
    int * buffer = (int *) calloc(DUMMY_DATA_SIZE, sizeof(*buffer));

    for (size_t i = 0; i < MAX_ITER; i += 1) {
        if (RANK_ROOT == comm_info.rank) {
            for (int other_rank = 0; other_rank < comm_info.size; other_rank += 1) {
                if (comm_info.rank == other_rank) {
                    continue;
                }

                MPI_Send(buffer, DUMMY_DATA_SIZE, MPI_INT, other_rank, DUMMY_DATA_TAG, comm_info.comm);
            }
        }
        else {
            MPI_Recv(buffer, DUMMY_DATA_SIZE, MPI_INT, RANK_ROOT, DUMMY_DATA_TAG, comm_info.comm, MPI_STATUS_IGNORE);
        }

        /* This emulates some calculations that all processes must complete */
        const size_t single_process_workload_ms = 128;
        usleep(1000 * single_process_workload_ms / comm_info.size);

        if (RANK_ROOT == comm_info.rank) {
            for (int other_rank = 0; other_rank < comm_info.size; other_rank += 1) {
                if (comm_info.rank == other_rank) {
                    continue;
                }

                MPI_Recv(
                        buffer, DUMMY_DATA_SIZE, MPI_INT,
                        other_rank, DUMMY_DATA_TAG, comm_info.comm, MPI_STATUS_IGNORE
                );
            }
        }
        else {
            MPI_Send(buffer, DUMMY_DATA_SIZE, MPI_INT, RANK_ROOT, DUMMY_DATA_TAG, comm_info.comm);
        }
    }

    free(buffer);

    return EXIT_SUCCESS;
}