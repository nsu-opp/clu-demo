#!/bin/bash
set -x

mpecc -mpilog -std=gnu99 -O3 -Wpedantic -Wall -Werror -Wextra -o clu-demo main.c
